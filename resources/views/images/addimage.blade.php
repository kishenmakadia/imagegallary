@extends('layouts.app')

@section('title','Images') 

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
           <div class="well" style="background-color: white">
                <form class="form-horizontal" action="{{ url('images') }}" method="post">
                {{ csrf_field() }}
                    <fieldset>
                        <legend>Add an image</legend>
                        @if (count($errors)>0)
                            <div class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                {{ $error }}
                            @endforeach
                            </div>
                        @endif
                        <div class="form-group">
                            <label class="col-lg-2 control-label ">Select Image</label>
                            <div class="col-lg-10">
                                <input type="file" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputCaption" class="col-lg-2 control-label">Caption</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" name="caption" placeholder="Write caption here...">
                            </div>
                        </div>
                            
                        <div class="form-group">
                            <label for="inputDescription" class="col-lg-2 control-label">Description</label>
                            <div class="col-lg-10">
                                <textarea class="form-control" rows="5" name="description" placeholder="Describe the image that you are uploading..."></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-10 col-lg-offset-2">
                                <button type="reset" class="btn btn-default">Cancel</button>
                                <button type="submit" class="btn btn-success">Submit</button>
                            </div>
                        </div>
                  </fieldset>
                </form>
           </div>
        </div>
    </div>
</div>
@endsection
