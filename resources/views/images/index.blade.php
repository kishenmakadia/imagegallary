@extends('layouts.app')

@section('title','Images') 

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Images</div>

                <div class="panel-body">
                    <center><a href="{{ url('images/create') }}" class="btn btn-info">Add Image</a></center>
                    <br>
                    <ul class="list-group">
                    @foreach ($images as $image)
                        @if ($image->user_id == Auth::user()->id)
                            <li class="list-group-item">
                            <span class="pull-right">{{$image->created_at->diffforHumans()}}</span><br>
                                Caption: <a href='{{ url("images/$image->id") }}'>{{ $image->caption }}</a><br>
                                Description: {{ $image->description }}
                            </li>
                        @endif
                    @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
