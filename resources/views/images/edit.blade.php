@extends('layouts.app')

@section('title','Image Edit') 

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Images</div>

                <div class="panel-body">
                    {{ $image }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
